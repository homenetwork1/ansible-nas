
install-local:
	ansible-galaxy install -r requirements.yml
run:
	ansible-playbook -i inventories/my-ansible-nas/inventory nas.yml -b -K
apt-update:
	ansible-playbook -i inventories/my-ansible-nas/inventory ./my-tasks/update-apt.yml -b -K
deb-update:
	ansible-playbook -i inventories/my-ansible-nas/inventory ./my-tasks/debain-packages.yml -b -K
setup-zfs:
	ansible-playbook -i inventories/my-ansible-nas/inventory ./my-tasks/setup-zfs.yml 
setup-rclone:
	ansible-playbook -i inventories/my-ansible-nas/inventory ./my-tasks/setup-rclone.yml 
setup-rsync-pull:
	ansible-playbook -i inventories/my-ansible-nas/inventory ./my-tasks/setup-rsync-pull.yml 
